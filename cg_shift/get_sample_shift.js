function get_data(number_of_rows, seats_per_row, row_pitch, passenger_mass, number_of_passengers, aircraft_mass, cg_aircraft, position_back_row, number_samples, first){
    var url = new URL('https://nathanvanthof.pythonanywhere.com/api');
    params = {
        'number_of_rows': number_of_rows, 
        'seats_per_row': seats_per_row, 
        'row_pitch': row_pitch, 
        'passenger_mass': passenger_mass,
        'number_of_passengers': number_of_passengers,
        'aircraft_mass': aircraft_mass, 
        'cg_aircraft': cg_aircraft, 
        'position_back_row': position_back_row,
        'number_samples': number_samples,
        'model': 'cg-shift-statistics'};

    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
    console.log(url);
    fetch(url)
    .then(function(response) {
        console.log(response);
        return response.json();
    })
    .then(function(myJson) {
        plot_sample_shift(myJson['random_samples'], first);
    })
}
