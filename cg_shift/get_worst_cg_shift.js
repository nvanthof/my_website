function get_data(number_of_rows, seats_per_row, row_pitch, passenger_mass, aircraft_mass, cg_aircraft, position_back_row, first){
    var url = new URL('https://nathanvanthof.pythonanywhere.com/api');
    params = {
        'number_of_rows': number_of_rows, 
        'seats_per_row': seats_per_row, 
        'row_pitch': row_pitch, 
        'passenger_mass': passenger_mass,
        'aircraft_mass': aircraft_mass, 
        'cg_aircraft': cg_aircraft, 
        'position_back_row': position_back_row,
        'model': 'cg-shift-worst-case'};

    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
    fetch(url)
    .then(function(response) {
        console.log(response);
        return response.json();
    })
    .then(function(myJson) {
        plot_worst_cg_shift(myJson['back_to_front'], myJson['front_to_back'], first);
    })
}
