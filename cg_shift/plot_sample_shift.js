var histogram;

function plot_sample_shift(samples, first){
    console.log(samples);
    let myChart = document.getElementById('myChart').getContext('2d');
    let minimum = Math.min.apply(Math, samples);
    let maximum = Math.max.apply(Math, samples);
    let bins = 20;
    let diff = maximum - minimum;


    var cgs = [];
    var totals = [];
    for (var i = 0; i <= bins; i++) {
        cgs.push(Math.round(10 * (minimum + diff * i / 20)) / 10);
        totals.push(0);
    }    
    
    for (var i = 0; i <= samples.length; i++){
        totals[Math.floor((samples[i] - minimum) * bins / diff)] ++;
    }

    for (var i = 0; i <= 20; i++) {
        totals[i] = Math.round(1000 * totals[i] / samples.length) / 10;
    }   

    if (histogram){
        histogram.destroy();
    }
    histogram = new Chart(myChart, {
        type:'bar', // bar, horizontalbar, pie, line, doughnut, radar, polarArea
        data:{
            labels: cgs,
            datasets:[{
                label: 'Percentage of cgs in bin',
                data: totals,
                fill: false,
                backgroundColor: 'rgba(255, 99, 132, 1)',
            }
            ]
        },
        options:{
            scales: {
              xAxes: [{
                display: false,
                barPercentage: 1.3,
                ticks: {
                    max: 3,
                }
             }, {
                display: true,
                ticks: {
                    autoSkip: false,
                    max: 4,
                }
              }],
              xAxes: [{
                  scaleLabel: {
                     display: true,
                     labelString: 'Center of graviy [m]'}}],
              yAxes: [{
                ticks: {
                  beginAtZero:true
                },
                scaleLabel: {
                    display: true,
                    labelString: 'probability'},
              }]
            }
        },
    });
}