function plot_worst_cg_shift(back_to_front, front_to_back){
    
    let myChart = document.getElementById('myChart').getContext('2d');
    var number_of_passengers = [];
    for (var i = 0; i <= back_to_front.length; i++) {
        number_of_passengers.push(i);
    }    


    let linechart = new Chart(myChart, {
        type:'line', // bar, horizontalbar, pie, line, doughnut, radar, polarArea
        data:{
            labels: number_of_passengers,
            datasets:[{
                label: 'Filling out back to front',
                data:  back_to_front,
                fill: false,
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)'
            },
            {
                label: 'Filling out front to back',
                data:  front_to_back,
                fill: false,
                backgroundColor: 'rgb(54, 162, 235)',
                borderColor: 'rgb(54, 162, 235)'
            }]
        },
        options:{
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Center of gravity (as measured from the rear) [m]'
                    }
                }],
                
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Number of passengers [-]'
                    }
                }]
            }     
        },
    });
}